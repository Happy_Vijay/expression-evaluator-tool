typedef struct nodec{
	char val;
	struct nodec *next;
}nodec;
typedef struct nodec* cstack;
void cinit(cstack *s);
void cpush(cstack *s, char val);
int cpop(cstack *s);
int cisEmpty(cstack *s);
int cisFull(cstack *s);
