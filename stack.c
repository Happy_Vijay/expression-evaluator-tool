#include<stdlib.h>
#include<stdio.h>
#include"stack.h"
void init(stack *p){
	*p = NULL;
}
void push(stack *p, int val){
	stack temp = (node *)malloc(sizeof(node));
	if(temp == NULL){
		fprintf(stderr, "Malloc Failed");
		return;
	}
	temp -> val = val;
	if(*p == NULL){
		*p = temp;
		temp -> next = NULL;
	}
	else{
		temp -> next = *p;
		*p = temp;
	}
}
int pop(stack *p){
	stack temp = *p;
	int val = temp -> val;
	*p = temp -> next;
	free(temp);
	return val;
}
int isEmpty(stack *p){
	if(*p == NULL){
		return 1;
	}
	else{
		return 0;
	}
}
int isFull(stack *p){
	return 0;		/* A Stack created by linked list is never full*/
}
