typedef struct node{
	int val;
	struct node *next;
}node;
typedef struct node *stack;
void init(stack *p);
void push(stack *p, int val);
int pop(stack *p);
int isEmpty(stack *p);
int isFull(stack *p);
