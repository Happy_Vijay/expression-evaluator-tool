#include"cstack.h"
#include<stdio.h>
#include<stdlib.h>
void cinit(cstack *s){
	*s = NULL;
}
void cpush(cstack *s, char val){
	cstack temp;
	temp = (cstack)malloc(sizeof(nodec));
	if(temp == NULL){
		fprintf(stderr,"Malloc Failed\n");
		exit(1);
	}
	temp -> val = val;
	if(*s == NULL){
		*s = temp;
		temp -> next = NULL;
	}
	else{
		temp -> next = *s;
		*s = temp;
	}
}
int cpop(cstack *s){
	cstack temp = *s;
	char val = temp -> val;
	*s = temp -> next;
	free(temp);
	return val;
}
int cisEmpty(cstack *s){
	if(*s == NULL){
		return 1;
	}
	else
		return 0;
}
int cisFull(cstack *s){
	return 0;
}
