    The user can enter arbitrary long bracketed expression which he wants to evaluate.
    The program does the task of tokenizing the input, thus extracting the infix expression from the user.
    Then the next task is to convert this infix expressions into postfix expression and then solves for this postfix expression.
    I didnt used any library functions, stack is user defined.